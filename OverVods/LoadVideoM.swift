//
//  LoadVideoM.swift
//  OverVods
//
//  Created by BlackWolf on 6/7/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol  LoadVideopro {
    func modelReTournamentMenu(data:Dictionary<String,Array<String>>)
    func modelReTournamentVid(data:Array<Dictionary<String,Array<String>>>)
    func modelReRecentVid(data:Array<Dictionary<String,Array<String>>>)
    func modelReFavoriteVid(data:Array<Dictionary<String,Array<String>>>)
}

class LoadVideoM {
    
    let url = "https://www.googleapis.com/youtube/v3/playlists"
    let urlVid = "https://www.googleapis.com/youtube/v3/playlistItems"
    
    
    let api = "AIzaSyBO_MlCA5aPMKKnyfXqkFO_XBPy0iy3XWw"
    let channelID_Basic = "UCpfBWhU12dbS5NS_3yS5NWA"
    let channelID_Live = ""
    var delegate:LoadVideopro?
    
    
    init() {
        
    }
    
    // Wanting Data  1.Title 2.playlist 3.thumbnails
    func loadPlayList(){
        
        
        
        // Alamofire request url from youtube.
        Alamofire.request(url,
                          method: .get,
                          parameters: getParameter(type:"playlist",plID:""),
                          encoding: URLEncoding.default)
            .responseData(completionHandler: { response in
                
                if response.result.value != nil{
                    
                    //variable
                    var tourName = Array<String>()
                    var playlistID = Array<String>()
                    var thumbnails = Array<String>()
                    var tourDate = Array<String>()
                    
                    var tourStore = Dictionary<String,Array<String>>()
                    
                    
                    
                    // json serialize by swiftyjson
                    let value = response.result.value
                    let json = JSON(data: value!)
                    
                    // assign json to JSON array
                    let allTour = json["items"].array
                    
                    // add data to array
                    for i in  0 ..< allTour!.count{
                        playlistID.append(allTour![i]["id"].stringValue)
                        tourName.append(allTour![i]["snippet"]["title"].stringValue)
                        tourDate.append(allTour![i]["snippet"]["publishedAt"].stringValue)
                        thumbnails.append(allTour![i]["snippet"]["thumbnails"]["standard"]["url"].stringValue)
                        
                    }
                    
                    // add array of data to store dictionary
                    tourStore["TournamentName"] = tourName
                    tourStore["TournamentThumb"] = thumbnails
                    tourStore["TournamentDate"] = tourDate
                    tourStore["TournamentPlaylist"] = playlistID
                    
                    // add tournament storage to tour list array
                    
                    
                    
                    self.delegate?.modelReTournamentMenu(data:tourStore)
                }
            })
        
        
    }
    
    // Wanting Data  1.Title 2.vidID 3.thumbnails
    func loadPlaylistVideo(playlistID:String){
        
        // Alamofire request url from youtube.
        Alamofire.request(urlVid,
                          method: .get,
                          parameters: getParameter(type:"vid",plID:playlistID),
                          encoding: URLEncoding.default)
            .responseData(completionHandler: { response in
                
                if response.result.value != nil{
                    
                    // initiate variable
                    var videoID = Array<String>()
                    var videoTitle = Array<String>()
                    var videoThumb = Array<String>()
                    
                    var videoStore = Dictionary<String,Array<String>>()
                    var videoList = Array<Dictionary<String,Array<String>>>()
                    
                    // json serialize by swiftyjson
                    let value = response.result.value
                    let json = JSON(data: value!)
                    
                    let allVid = json["items"].array
                    
                    for i in 0 ..< allVid!.count{
                        videoTitle.append(allVid![i]["snippet"]["title"].stringValue)
                        videoID.append(allVid![i]["snippet"]["resourceId"]["videoId"].stringValue)
                        videoThumb.append(allVid![i]["snippet"]["thumbnails"]["standard"]["url"].stringValue)
                        
                        videoStore["VideoTitle"] = videoTitle
                        videoStore["VideoID"] = videoID
                        videoStore["VideoThumb"] = videoThumb
                        
                        videoList.append(videoStore)
                        
                        print(videoList)
                    }
                    
                    
                    
                }
                
                
            })
        
    }
    
    
    func getParameter(type:String,plID:String) -> Parameters{
        // Create parameter for api call
        
        var para = Parameters()
        
        switch type {
        case "playlist":
            para = ["part":"snippet",
                    "channelId": channelID_Basic,
                    "maxResults":50,
                    "key": api]
            return para
            
        case "vid":
            para = ["part":"snippet",
                    "playlistId": plID,
                    "maxResults":50,
                    "key": api]

            return para
        default:
             para = ["part":"snippet",
            "channelId": channelID_Basic,
            "maxResults":50,
            "key": api]
            return para
        }
    }
}


