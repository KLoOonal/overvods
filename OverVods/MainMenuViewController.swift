//
//  MainMenuViewController.swift
//  OverVods
//
//  Created by Blackwolf on 6/14/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {

    var nextMenu:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ClickButton(_ sender: Any) {
        
        nextMenu = "Tournament"
        performSegue(withIdentifier: "nextMenu", sender: self)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "nextMenu"){
         let nextview = segue.destination as! PlaylistTableTableViewController
        
        nextview.titleTable = nextMenu
        }
        
    }
    

}
