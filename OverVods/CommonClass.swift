//
//  CommonClass.swift
//  OverVods
//
//  Created by Blackwolf on 6/15/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import Foundation
import UIKit

struct  CommonClass {

    
    public func tableViewBackground(picName:String,viewSize:UIView) -> UIImageView{
        let backgroundImage = UIImage(named: picName)
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFill
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = viewSize.frame
        imageView.addSubview(blurView)
    
        return imageView
    }
}
