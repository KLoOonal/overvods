//
//  TableViewCell.swift
//  OverVods
//
//  Created by Blackwolf on 6/20/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import UIKit
import YouTubePlayer_Swift

class CustomTableViewCell2: UITableViewCell {

  
    @IBOutlet weak var yotubePlayer: YouTubePlayerView!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func loadVidCell(vid:String){
        yotubePlayer.loadVideoID(vid)
    }

}
