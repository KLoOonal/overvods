//
//  PlaylistVM.swift
//  OverVods
//
//  Created by Blackwolf on 6/16/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import Foundation

protocol PlayListTableProtocal{
    
    func setDatatoView(data:Dictionary<String,Array<String>>)
    func reloadPage()
}

class  PlaylistVM: LoadVideopro{
    
    var delegate:PlayListTableProtocal?
    var model = LoadVideoM()
    
    var playList = Dictionary<String, Array<String>>()
    var videoList = Array<Dictionary<String,Array<String>>>()
    
    var playlistID = Array<String>()
    var playlistName = Array<String>()
    var playlistImage = Array<String>()
    var playlistDate = Array<String>()
    
    
    // MARK: - Method call from view controller
    
    func tournametMenu() {
        setDelegate()
        model.loadPlayList()
    }
    
    func tournamentVid(playlist:String){
        setDelegate()
        model.loadPlaylistVideo(playlistID: playlist)
    }
    
    func recentVid(){
        setDelegate()
        
    }
    
    func favoriteVid(){
        setDelegate()
        
        
    }
    
    private func setDelegate(){
    model.delegate = self
    }
    
    // MARK: - finished function
    
    func modelReTournamentMenu(data:Dictionary<String,Array<String>>){
        playList = data;
     
        
        // substring for date
        for var j in 0 ..< playList["TournamentDate"]!.count{
            playList["TournamentDate"]![j] = playList["TournamentDate"]![j].substring(to: playList["TournamentDate"]![j].index(playList["TournamentDate"]![j].startIndex, offsetBy: 10))
        }
        
        
        delegate?.setDatatoView(data: playList)
        delegate?.reloadPage()
    }
    
    func modelReTournamentVid(data:Array<Dictionary<String,Array<String>>>){
        videoList = data
    }
    
    func modelReRecentVid(data:Array<Dictionary<String,Array<String>>>){
        
    }
    
    func modelReFavoriteVid(data:Array<Dictionary<String,Array<String>>>){
        
    }
}
