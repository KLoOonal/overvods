//
//  PlaylistTableTableViewController.swift
//  OverVods
//
//  Created by Blackwolf on 6/15/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import UIKit



class PlaylistTableTableViewController: UITableViewController , PlayListTableProtocal {
   


    var titleTable:String = String()
    var viewModel = PlaylistVM()
    
    
    var playlistID = Array<String>()
    var playlistImage = Array<String>()
    var playlistName = Array<String>()
    var playlistCount:Int32 = 0
    var playlistDate = Array<String>()
 
   /*/ Dependency inject part
    var playlistID = ["5","4","3","2"]
    var playlistImage = ["","","",""]
    var playlistName = ["five","four","three","two"]
    var playlistDate = ["","","",""]
    var playlistCount:Int32 = 0
    */
    override func viewWillAppear(_ animated: Bool)
    {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        
        // Add a background view to the table view
        let common = CommonClass()
        self.tableView.backgroundView = common.tableViewBackground(picName: "background",viewSize: self.view)
        self.navigationItem.title = titleTable
       
        viewModel.delegate = self
        viewModel.tournametMenu()
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // get rid of extracell
        //tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Int(playlistCount)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor(white: 1, alpha: 0.2)
    }

   

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        
        cell.name.text = playlistName[indexPath.row]
        do{
        try cell.cellImage.image = UIImage(data: NSData(contentsOf: NSURL(string: playlistImage[indexPath.row])! as URL) as Data)
        } catch{}
        cell.date.text = playlistDate[indexPath.row]
  
            
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "videoList") as! VideolistTableViewController
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 */

    
    
    // MARK: - Protocal function
    
    
    func setDatatoView(data: Dictionary<String,Array<String>>){
        
            playlistName =  data["TournamentName"]!
            playlistID = data["TournamentPlaylist"]!
            playlistImage = data["TournamentThumb"]!
            playlistDate = data["TournamentDate"]!
            playlistCount = Int32(playlistName.count)
    }
    
    func reloadPage(){
        tableView.reloadData()
    }
}
