//
//  CustomTableViewCell.swift
//  OverVods
//
//  Created by Blackwolf on 6/16/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cellImage: UIImageView!
    
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //name.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
