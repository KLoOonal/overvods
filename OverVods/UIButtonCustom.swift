//
//  UIButtonCustom.swift
//  OverVods
//
//  Created by BlackWolf on 6/1/2560 BE.
//  Copyright © 2560 BlackWolf. All rights reserved.
//

import UIKit

@IBDesignable
class UIButtonCustom: UIButton {

    @IBInspectable var connerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = connerRadius;
            
        }
    
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            self.layer.borderWidth = borderWidth;
            
        }
        
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor;
            
        }
    }
    
    
}
